#!/bin/bash

while read line
do
#    echo $line
    arr=( `echo $line | tr -s ',' ' '`)
#    echo ${arr[0]}.${arr[1]}.${arr[2]}
    notify-send -t 10000 ${arr[0]} `~/script/google-authenticator.py ${arr[1]}`
done < ~/fuse/encfs/.2auths

