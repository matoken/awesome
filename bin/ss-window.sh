#!/bin/sh
xwininfo |grep '^xwininfo: Window id:' | awk '{print $4}' | xargs -I{} import -window {} ~/Pictures/`date +%Y%m%d_%H:%m:%S-$$.jpg`
