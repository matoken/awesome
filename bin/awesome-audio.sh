#!/bin/bash

#-- ALSA Volume controle script
# http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FWindowManager%2Fawesome%2F3.5#ba87c638

MUTE=`amixer -c 0 get Master|tail -1|cut -d '[' -f 4|sed s/\]//`

if [ $MUTE = "on" ] ; then
  ICON="/usr/share/icons/ContrastHigh/scalable/status/audio-volume-medium.svg"
else
  ICON="/usr/share/icons/ContrastHigh/scalable/status/audio-volume-muted.svg"
fi

case "$1" in
  "XF86AudioMute" ) 
    if [ $MUTE = "on" ] ; then
      amixer -q -c 0 set Master mute
      xset -b
      echo -e "🔊☓\nmute!"
      notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-muted.svg' mute "☓"
    else
      amixer -q -c 0 set Master unmute
      amixer -q -c 0 set Speaker unmute
      amixer -q -c 0 set Headphone unmute
      xset b
      echo -e "🔊\nunmute!"
      amixer -c 0 get Master | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-high.svg' numute
    fi
  ;;
  "XF86AudioRaiseVolume" ) 
    amixer -c 0 set Master 2dB+ | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i $ICON "Vol ⤴"
  ;;
  "XF86AudioLowerVolume" ) 
    amixer -c 0 set Master 2dB- | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i $ICON "Vol ⤵"
  ;;
esac

